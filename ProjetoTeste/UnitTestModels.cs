using System;
using Xunit;
using MyFinance.Models;
namespace ProjetoTeste
{
    public class UnitTestModels
    {
        [Fact]
        public void TesteLoginUsuario()
        {
            UsuarioModel usuarioModel = new UsuarioModel();
            usuarioModel.Email = "sidney.moraes1974@gmail.com";
            usuarioModel.Senha = "123456789";
            Assert.True(usuarioModel.ValidarLogin());
        }

        [Fact]
        public void TesteRegistraUsuario()
        {
            UsuarioModel usuarioModel = new UsuarioModel();
            usuarioModel.Nome = "Vanessa";
            usuarioModel.Email = "vanessa-sabino2011@hotmail.com";
            usuarioModel.Senha = "teste@12345";
            usuarioModel.Data_Nascimento = DateTime.Now.ToString();
            usuarioModel.RegistrarUsuario();
            Assert.True(usuarioModel.ValidarLogin());
        }
    }
}
