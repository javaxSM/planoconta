﻿using Microsoft.AspNetCore.Http;
using MyFinance.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace MyFinance.Models
{
    public class TransacaoModel
    {

        public int Id { get; set; }
        [Required(ErrorMessage = "Informe a Data!")]
        public string Data { get; set; }
        public string Tipo { get; set; }
        [DataType(DataType.Currency)]
        public double Valor { get; set; }
        [Required(ErrorMessage = "Descrição necessária")]
        public string Descricao { get; set; }
        public int Conta_id { get; set; }
        public string NomeConta { get; set; }
        public int Plano_Contas_Id { get; set; }
        public string DescricaoPlanoConta { get; set; }
        public IHttpContextAccessor HttpContextAccessor { get; set; }

        public TransacaoModel(IHttpContextAccessor httpContextAccessor)
        {
            HttpContextAccessor = httpContextAccessor;
        }

        public TransacaoModel()
        {

        }

        public List<TransacaoModel> ListaTransacao()
        {
            List<TransacaoModel> Lista = new List<TransacaoModel>();
            TransacaoModel item;

            string IdUsuarioLogado = HttpContextAccessor.HttpContext.Session.GetString("IdUsuarioLogado");

            string sql = $"select t.id, t.data, t.tipo, t.valor, t.descricao as historico, " +
                        " t.conta_id, c.NomeConta as conta, t.plano_contas_id, p.descricao as Plano_conta " +
                        " from transacao as t inner join conta as c ON T.conta_id = c.id " +
                        $" INNER JOIN Plano_Contas as p ON t.plano_contas_id = p.id where t.Usuario_id = {IdUsuarioLogado} order by t.data desc";


            DAO objDal = new DAO();
            DataTable dt = objDal.RetDataTable(sql);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                item = new TransacaoModel();
                item.Id = int.Parse(dt.Rows[i]["ID"].ToString());
                item.Data =  DateTime.Parse( dt.Rows[i]["Data"].ToString()).ToString("dd/MM/yyyy");
                item.Descricao = dt.Rows[i]["Historico"].ToString();
                item.Valor = double.Parse(dt.Rows[i]["valor"].ToString());
                item.Conta_id = int.Parse(dt.Rows[i]["Conta_id"].ToString());
                item.NomeConta = dt.Rows[i]["Conta"].ToString();
                item.Plano_Contas_Id = int.Parse(dt.Rows[i]["Plano_Contas_Id"].ToString());
                item.DescricaoPlanoConta = dt.Rows[i]["Plano_conta"].ToString();
                item.Tipo = dt.Rows[i]["Tipo"].ToString();
                Lista.Add(item);
            }

            return Lista;
        }

        public void Excluir(int id)
        {
            new DAO().ExecutaComandoSQL("DELETE FROM TRANSACAO WHERE ID = " + id);
        }

        public void Insert()
        {
            string sql = "";

            if (Id == 0)
            {
                string IdUsuarioLogado = HttpContextAccessor.HttpContext.Session.GetString("IdUsuarioLogado");
                sql = $"INSERT INTO TRANSACAO (DATA, TIPO, DESCRICAO, VALOR, CONTA_ID, PLANO_CONTAS_ID, USUARIO_ID) VALUES" +
                    $" ('{DateTime.Parse(Data).ToString("yyyy/MM/dd")}', '{Tipo}', '{Descricao}', '{Valor}', '{Conta_id}', '{Plano_Contas_Id}', '{IdUsuarioLogado}')";
            }
            else
            {
                string IdUsuarioLogado = HttpContextAccessor.HttpContext.Session.GetString("IdUsuarioLogado");
                sql = $"UPDATE TRANSACAO SET Data = '{DateTime.Parse(Data).ToString("yyyy-MM-dd")}', " +
                    $" TIPO = '{Tipo}', DESCRICAO = '{Descricao}', Valor= '{Valor}', CONTA_ID = '{Conta_id}', PLANO_CONTAS_ID = '{Plano_Contas_Id}'  " +
                    $"WHERE USUARIO_ID = '{IdUsuarioLogado}' AND ID = {Id}";
            }

            DAO objDAL = new DAO();  // Objeto de acesso aos dados (BD).

            objDAL.ExecutaComandoSQL(sql);
        }

        public List<TransacaoModel> ListarTransacao()
        {
            List<TransacaoModel> Lista = new List<TransacaoModel>();
            TransacaoModel item;

            //Utilizado pela view Extrato
            string filtro = "";

            if ((Data != null))
            {
                filtro += $" and t.data >= '{DateTime.Parse(Data).ToString("yyyy/MM/dd")}' and t.data <= '{DateTime.Parse(Data).ToString("yyyy/MM/dd")}'";
            }
            
            if(Tipo != null)
            {
                if(Tipo != "A")
                {
                    filtro += $" and t.tipo = '{Tipo}'";
                }
            }

            if(Conta_id != 0)
            {
                filtro += $" and t.conta_id = '{Conta_id}' ";
            }

            //fim

            string IdUsuarioLogado = HttpContextAccessor.HttpContext.Session.GetString("IdUsuarioLogado");


            string sql = $"select t.id, t.data, t.tipo, t.valor, t.descricao as historico, " +
                        " t.conta_id, c.NomeConta as conta, t.plano_contas_id, p.descricao as Plano_conta " +
                        " from transacao as t inner join conta as c ON T.conta_id = c.id " +
                        $" INNER JOIN Plano_Contas as p ON t.plano_contas_id = p.id where t.Usuario_id = {IdUsuarioLogado} {filtro} order by t.data desc";


            DAO objDal = new DAO();
            DataTable dt = objDal.RetDataTable(sql);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                item = new TransacaoModel();
                item.Id = int.Parse(dt.Rows[i]["ID"].ToString());
                item.Data = DateTime.Parse(dt.Rows[i]["Data"].ToString()).ToString("dd/MM/yyyy");
                item.Descricao = dt.Rows[i]["Historico"].ToString();
                item.Valor = double.Parse(dt.Rows[i]["valor"].ToString());
                item.Conta_id = int.Parse(dt.Rows[i]["Conta_id"].ToString());
                item.NomeConta = dt.Rows[i]["Conta"].ToString();
                item.Plano_Contas_Id = int.Parse(dt.Rows[i]["Plano_Contas_Id"].ToString());
                item.DescricaoPlanoConta = dt.Rows[i]["Plano_conta"].ToString();
                item.Tipo = dt.Rows[i]["Tipo"].ToString();
                Lista.Add(item);
            }

            return Lista;
        }
        public TransacaoModel CarregarRegistro(int? id)
        {
            TransacaoModel item;

            string IdUsuarioLogado = HttpContextAccessor.HttpContext.Session.GetString("IdUsuarioLogado");


            string sql = $"select t.id, t.data, t.tipo, t.valor, t.descricao as historico, " +
                        " t.conta_id, c.NomeConta as conta, t.plano_contas_id, p.descricao as Plano_conta " +
                        " from transacao as t inner join conta as c ON T.conta_id = c.id " +
                        $" INNER JOIN Plano_Contas as p ON t.plano_contas_id = p.id where t.Usuario_id = {IdUsuarioLogado} and t.id = '{id}'";


            DAO objDal = new DAO();
            DataTable dt = objDal.RetDataTable(sql);

                item = new TransacaoModel();
                item.Id = int.Parse(dt.Rows[0]["ID"].ToString());
                item.Data = DateTime.Parse(dt.Rows[0]["Data"].ToString()).ToString("dd/MM/yyyy");
                item.Descricao = dt.Rows[0]["Historico"].ToString();
                item.Valor = double.Parse(dt.Rows[0]["valor"].ToString());
                item.Conta_id = int.Parse(dt.Rows[0]["Conta_id"].ToString());
                item.NomeConta = dt.Rows[0]["Conta"].ToString();
                item.Plano_Contas_Id = int.Parse(dt.Rows[0]["Plano_Contas_Id"].ToString());
                item.DescricaoPlanoConta = dt.Rows[0]["Plano_conta"].ToString();
                item.Tipo = dt.Rows[0]["Tipo"].ToString();
            
            return item;
        }
    }

    public class Dashboard
    {
        public double Total { get; set; }
        public string PlanoConta { get; set; }
        public IHttpContextAccessor HttpContextAccessor { get; set; }

        public Dashboard()
        {

        }

        public Dashboard(IHttpContextAccessor httpContextAccessor)
        {
            HttpContextAccessor = httpContextAccessor;
        }


        public List<Dashboard> dashboardsPie()
        {
            List<Dashboard> lista =   new List<Dashboard>();
            Dashboard item;

            string IdUsuarioLogado = HttpContextAccessor.HttpContext.Session.GetString("IdUsuarioLogado");
            string sql = "SELECT sum(t.valor) as total, p.descricao FROM transacao as t INNER JOIN plano_contas as p on t.plano_contas_id = p.id WHERE " +
                            $"t.tipo = 'D' and t.Usuario_ID = {IdUsuarioLogado} group by p.descricao; ";


            DAO objDAL = new DAO();
            DataTable dt = new DataTable();
            dt = objDAL.RetDataTable(sql);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                item = new Dashboard();
                item.Total = double.Parse(dt.Rows[i]["Total"].ToString());
                item.PlanoConta = dt.Rows[i]["Descricao"].ToString();
                lista.Add(item);
            }

            return lista;
        }
    }
}
